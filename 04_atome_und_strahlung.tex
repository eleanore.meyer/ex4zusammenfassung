\section{Atome und Strahlung}
\subsection{Übergangswahrscheinlichkeiten}
Die Übergangswahrscheinlichkeiten $i\rightarrow k$ werden durch die Einstein-Koeffizienten
$A_{ik}, \allowbreak B_{ik}, \allowbreak B_{ki}$ bestimmt, sodass:
\begin{itemize}
  \item \textbf{Absorption}: Der Prozess ist $h\nu + E_k \rightarrow E_i$
    \[W_{ki} = B_{ki} \cdot w_\nu (\nu)\]

  \item \textbf{Stimulierte/induzierte Emission}: Für ein einlaufendes Photon wird ein weiteres zu dem
    einlaufenden ``identisches'' Photon (also gleiche Richtung, Phase, \dots) emittiert, d.h.
    $h\nu + E_i \rightarrow h\nu + h\nu + E_k$
    \[W^\mathrm{ind}_{ik} = B_{ik}\cdot w_\nu (\nu)\]

  \item \textbf{Spontane Emission}: $E_i \rightarrow E_k + h\nu$
    \[W^\mathrm{spont}_{ik} = A_{ik}\]
\end{itemize}
mit der spektralen Energiedichte $w_\nu (\nu) = n(\nu) h \nu$ um das betrachtete Atom.
Das Verhältnis von induzierter zu spontaner Emission $\frac{W^\mathrm{ind}_{ik}}{W^\mathrm{spont}_{ik}} = n(\nu)$
ist die Anzahl $n(\nu)$ der Photonen in der jeweiligen Mode.

Für kleine Frequenzen gilt zudem
\[B_{ik} = \frac{g_k}{g_i} \cdot B_{ki}\]
, wobei $g_k, g_i$ den jeweiligen ``Degeneriertheitsgeraden'' entsprechen.

Jedem Übergang $i \rightarrow k$ lässt sich ein quantenmechanisches Dipolmoment $\vec p_{ik}$
, auch \textbf{Matrixelement} genannt, welches
gemittelt
\[\braket{\vec p} = \int \! \psi_i^* \vec r \psi_k \, \mathrm d\vec r\]
beträgt zuordnen. Dann lässt sich durch das Gleichsetzen der von solchen Dipolen abgestrahlen Leistungen
mit der Spontanen Emission der Koeffizient $A_{ik}$ bestimmen.


\highlightbox{Auswahlregeln für Übergänge}{
  \begin{itemize}
    \item \textbf{Magnetische Quantenzahl}:
      \begin{align*}
        \Delta m = 0   &\iff \text{lin. polar. Licht} \\
        \Delta m \pm 1 &\iff \text{zirk. polar. Licht}
      \end{align*}

    \item \textbf{Drehimpulsquantenzahl}: Die Drehimpulsquantenzahl muss sich aufgrund des Photonspins
      $1\hbar$ um $\Delta = \pm 1$ verändern.

    \item \textbf{Spin}: $\Delta S = 0$

    \item \textbf{Gesamtdrehimpuls}: Für $\vec J = \vec L + \vec S$ ist $\Delta J = -1,0,1$.
      Wenn jedoch $J= 0$ dann $\Delta J \neq 0$.
  \end{itemize}
}

\subsubsection{Lebensdauer angeregter Zustände}
\label{sec:strahlung:lebensdauer}
Die Gesamtwahrscheinlichkeit das ein Photon durch spontane Emission seine Anregung verliert ist
im Zustand $i$ gegeben durch:
\[A_k = \sum_k A_{ik}\]

Wenn $R_i$ zudem die ``Emission durch Stoß''-Wahrscheinlichkeit ist, so folgt die Anzahl der angeregten
Elektrone der Verteilung:
\[N(t) = N_0 \cdot e^{- (A_i + R_i)t}\]
mit der mittleren/effektiven Lebensdauer:
\[\tau = \frac{1}{A_1 + R_i}\]

$R_i$ lässt sich für zwei Gase mit der mittleren Relativgeschwindigkeit $\overline v_{AB}$ bestimmen zu:
\begin{align*}
  R_i &= n_B \cdot \overline v_{AB} \cdot \sigma_i^\mathrm{inel.}\\
\end{align*}


\subsection{Linienverbreiterung}
Verschiedenste Effekte führen dazu, dass eine Spektrallinie \emph{nicht} beliebig fein aufgelöst sein kann.
\begin{itemize}
  \item \textbf{Natürliche Linienbreite}:
    Aufgrund der begrenzten mittleren Lebenszeit ergibt sich nach der Heissenbergschen Unschärferelation
    eine natürliche Linienbreite.
    \[\Delta E \cdot \Delta t \geq \hbar \iff \Delta E \geq \frac{h}{2\pi \tau} = \frac{1}{2\pi\tau}\]

  \item \textbf{Dopplerverbreiterung}: Verbreiterung durch den Dopplereffekte und u.a. der
    thermischen Geschwindigkeiten der Atome bedingt. Die Halbwertsbreite ist:
    \[\SI{7.16e-7}{} \cdot \nu_0 \cdot \sqrt{\frac TM}\si{\hertz}\]
    , wobei $M$ die molare Masse in $\si{\gram\per\mol}$ bezeichne.

  \item \textbf{Stoßverbreiterung}: Die Stoßverbreiterung ist durch zwei Sachen bedingt:
    \begin{enumerate}
      \item Eine Verringerung der Lebensdauer durch inelastische Stöße
        und damit einhergehende erhöhung der natürlichen Linienbreite.
        Siehe auch \cref{sec:strahlung:lebensdauer}.

      \item Bei elastischen Stößen nähern sich die Atome an, und die Energieniveaus verschieben sich.
        Die entstehende Linienverbreiterung und verschiebung ist proportional zum Druck.
    \end{enumerate}
\end{itemize}


\subsection{Röntgenstrahlung}
Röntgenstrahlung kann erzeugt werden durch eine ``Beschleunigungsröhre'', in welcher Elektronen aus einer
Kathode ausgelöst werden, eine starke Beschleunigung unterlaufen, und dann auf ein Anodenmaterial
zulaufen.

Das Spektrum verfügt über kontinuierliche Anteile erzeugt durch die Bremsstrahlung,
als auch über charakteristische Anteile welche durch Stoßionisation der Elektrone im Anodenmaterial und
anschließender Rekombination bedingt sind.

Für die charakteristische Anteile unterscheidet man bestimmte Linien $K_\alpha, \allowbreak K_\beta,
\allowbreak L_\alpha, \allowbreak \ldots$.
Der Großbuchstabe gibt an aus welcher Schale ein Elektron ionisiert wurde und der Index aus welcher
Schale das Rekombinationselektron stammt. Z.B. $K_\beta$: Rekombination von der $M$ zur $K$-Schale.

Die Abschwächung von Röntgenstrahlung erfolgt mithilfe des \textbf{Abschwächungskoeffizient} $\mu$.
Die Abschwächung selbst gehorcht der exponentiellen Gesetzmäßigkeit
\[P(x) = P_0 \cdot e^{-\mu x}.\]
Die Abschwächung setzt sich aus Streuung (Compton-Effekt), Absorption (Photoeffekt) und Paarbildung
(Paarbildung) zusammen.
Da die Abschwächung stark vo der Dichte des Materials abhängt definiert man den
\textbf{Masseabschwächungskoeffizienten} $\kappa$ mit:
\[\kappa = \frac \mu \rho.\]

Im Absorptionsspektrum tretten bestimmte Kanten auf, an denen die Absorption sprunghaft ansteigt.
Diese korrespondieren zu Schalen $K,L,M,\dots$, so dass die Strahlung ab einem bestimmten Energiewerte
zusätzlich in der Lage ist Elektronen aus der nächst innerliegenden Schale ``rauszuschlagen''.
